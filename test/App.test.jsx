import { render } from '@testing-library/react';
import { describe, test, expect } from 'vitest';

const App = () => <div>learn react</div>

describe('Application', function () {
    test('renders learn react link', () => {
        const { getByText } = render(<App />);
        const linkElement = getByText(/learn react/i);
        expect(linkElement).toBeInTheDocument();
    });
});
