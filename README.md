# Application Basket

* Notre projet est une application qui permet a des clubs de basket d'envoyer des tactiques de jeux à leurs equipes.
* Editeur et visualisateur interne des tactiques de jeux.
* Les administrateurs (coachs) d'une equipe peuvent diffuser leurs tactiques à leurs élèves.
* Les élèves peuvent aussi proposer leurs propres tactiques que les coachs peuvent à leurs tour approuver. 

## Construire l'application

Ce projet utilise NPM comme gestionnaire de paquets, mais vous pouvez remplacer NPM par Yarn ou PNPM.

Une fois le dépôt cloné :

```bash
npm install # Installe les dépendances
npm run build # Construit l'application
npm run dev # Démarre le serveur de développement
npm run test # Lance les tests
```

## Contribuer
Dans le code ou dans la documentation, merci d'ecrire en anglais si possible
