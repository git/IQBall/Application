#!/usr/bin/bash

apt install apksigner > /dev/null

if [ $DRONE_BRANCH = "dev" ]; then
  ARTIFACT_TYPE="debug"
elif [ $DRONE_BRANCH = "production" ]; then
  ARTIFACT_TYPE="release"
fi

apksigner sign --ks /home/maxime/server/TBasket/application/android/tbasket.keystore app-$ARTIFACT_TYPE-unsigned.apk &&
 mv app-$ARTIFACT_TYPE-unsigned.apk tbasket-$ARTIFACT_TYPE.apk &&
 echo "apk has been signed."

echo "android-sign.sh done."