import react from '@vitejs/plugin-react';
import { defineConfig } from 'vitest/config';
import fs from 'fs/promises';

// https://vitejs.dev/config/
export default defineConfig({
    esbuild: {
        loader: "jsx",
        include: /(src|test)\/.*\.jsx?$/,
        exclude: [],
    },
    build: {
        target: 'es2021',
    },
    test: {
        globals: true,
        environment: 'jsdom',
        setupFiles: './test/setupTests.js'
    },
    plugins: [react()],
    optimizeDeps: {
        esbuildOptions: {
            plugins: [
                {
                    name: 'load-js-files-as-jsx',
                    setup(build) {
                        build.onLoad({ filter: /(src|test)\/.*\.js$/ }, async (args) => ({
                            loader: 'jsx',
                            contents: await fs.readFile(args.path, 'utf8'),
                        }));
                    },
                },
            ],
        },
    },
});
