import Register from './components/register/Register';
import Login from "./components/login/Login";
import {MainPage} from "./components/main/Main";
import {BrowserRouter, Routes, Route} from "react-router-dom";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/main" element={<MainPage/>} />
                    <Route path="/main/:id" element={<MainPage/>} />
                    <Route path="/*" element={<MainPage />} />
                </Routes>
            </BrowserRouter>
        </div>
    );
}
export default App;
