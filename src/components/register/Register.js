import '../../css/login.css'
import { library } from '@fortawesome/fontawesome-svg-core';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock, faEnvelope, faUser } from '@fortawesome/free-solid-svg-icons';

library.add(faLock);
library.add(faEnvelope);
library.add(faUser);

export default function Register() {
    return (
        <>
            <div className={"body"}>
                <div className="container">
                    <form action="/signup" method="POST">
                        <h3>S'inscrire :</h3>
                        <div className="Input">
                            <span>Adresse e-mail</span>
                            <div className="box">
                                <div className="icon">
                                    <FontAwesomeIcon icon="envelope" />
                                </div>
                                <input type="input" className="form__field" placeholder="email" name="email" id='email'
                                       required/>
                            </div>
                        </div>
                        <div className="Input">
                            <span>Nom</span>
                            <div className="box">
                                <div className="icon">
                                    <FontAwesomeIcon icon="user" />
                                </div>
                                <input type="input" placeholder="nom" name="name" id='name' required/>
                            </div>
                        </div>
                        <div className="Input">
                            <span>Prénom</span>
                            <div className="box">
                                <div className="icon">
                                    <FontAwesomeIcon icon="user" />
                                </div>
                                <input type="input" placeholder="Prénom" name="forename" id='forename' required/>
                            </div>
                        </div>

                        <div className="Input">
                            <span>Mot de passe</span>
                            <div className="box">
                                <div className="icon">
                                    <FontAwesomeIcon icon="lock" />
                                </div>
                                <input type="password" className="form__field" placeholder="mot de passe" name="password"
                                       id="password"
                                       required/>
                            </div>
                        </div>
                        <div className="Input">
                            <span>Confirmer le mot de passe</span>
                            <div className="box">
                                <div className="icon">
                                    <FontAwesomeIcon icon="lock" />
                                </div>
                                <input type="password" className="form__field" placeholder="mot de passe" name="cpassword"
                                       id="cpassword" required/>
                            </div>
                        </div>
                        <div className="Input">
                            <div className="box">
                                <input type="submit" value="S'inscrire"/>
                            </div>
                        </div>
                        <a href="/login">J'ai déjà un compte</a>
                    </form>
                </div>
            </div>
        </>

    )
}
