import '../../css/login.css'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function Login() {
    return (
        <>
            <div className={"body"}>
                <div className="container">
                    <form action="/login" method="POST">
                        <h3>Se connecter :</h3>
                        <div className="Input">
                            <span>Identifiant (eMail)</span>
                            <div className="box">
                                <div className="icon">
                                    <FontAwesomeIcon icon="envelope" />
                                </div>
                                <input type="input" className="form__field" placeholder="email" name="email" id='email'
                                       required/>
                            </div>
                        </div>
                        <div className="Input">
                            <span>Mot de passe</span>
                            <div className="box">
                                <div className="icon">
                                    <FontAwesomeIcon icon="lock" />
                                </div>
                                <input type="password" className="form__field" placeholder="mot de passe" name="password"
                                       id="password" required/>
                            </div>
                        </div>
                        <label>
                            <input type="checkbox" value="se souvenir de moi" id={"remember me"} name={"remember me"}/>
                            se souvenir de moi
                        </label>
                        <div className="Input">
                            <div className="box">
                                <input type="submit" value="Se connecter"/>
                            </div>
                        </div>
                        <div className={"Href"}>
                            <a href="/login">J'ai oublier mon mot de passe</a>
                            <a href="/register">Je n'ai pas encore de compte</a>
                        </div>

                    </form>
                </div>
            </div>
        </>
    )
}

