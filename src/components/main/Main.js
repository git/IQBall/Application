import '../../css/main.css'

export function MainPage() {
    return (<>
        <header>
            <div className="header-left">
                <img src="/img/logo2.png" alt="" />
            </div>
            <div className="header-center">
                <h2>Bienvenue, Prénom Nom</h2>
            </div>
            <div className="header-right">
                <button className="submit-button">Déconnexion</button>
            </div>
        </header>
        <div id="teams-section">
            <div id="team-list-h">
                <h2>Mes équipes</h2>
            </div>
            <div id="teams-list">
                {/*afficher la liste des équipes*/}
                <div className="team-item">
                    <img src="/img/logo2.png" alt="" />
                    <p className="team-name">Equipe 1</p>
                </div>
                <div className="team-item">
                    <img src="/img/logo2.png" alt="" />
                    <p className="team-name">Equipe 2</p>
                </div>
                <div className="team-item">
                    <img src="/img/logo2.png" alt="" />
                    <p className="team-name">Equipe 3</p>
                </div>
                <div className="team-item">
                    <img src="/img/logo2.png" alt="" />
                    <p className="team-name">Equipe 3</p>
                </div>
                <div className="team-item">
                    <img src="/img/logo2.png" alt="" />
                    <p className="team-name">Equipe 3</p>
                </div>
                <div className="team-item">
                    <img src="/img/logo2.png" alt="" />
                    <p className="team-name">Equipe 3</p>
                </div>
            </div>
        </div>
        <div id="schemas-section">
            <div id="schemas-list-h">
                <h2>Mes Schémas</h2>
            </div>
            <div id="schemas-list">
                {/*afficher la liste des Schémas*/}
                <div className="schemas-item">
                    <img src="/img/Visualiseur.png" alt="" />
                    <p className="schemas-name">Schéma 1</p>
                </div>
                <div className="schemas-item">
                    <img src="/img/Visualiseur.png" alt="" />
                    <p className="schemas-name">Schéma 2</p>
                </div>
                <div className="schemas-item">
                    <img src="/img/Visualiseur.png" alt="" />
                    <p className="schemas-name">Schéma 3</p>
                </div>
                <div className="schemas-item">
                    <img src="/img/Visualiseur.png" alt="" />
                    <p className="schemas-name">Schéma 3</p>
                </div>
            </div>
        </div>
    </>
)
}

