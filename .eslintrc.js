module.exports = {
    parserOptions: {
        ecmaVersion: 2021,
        sourceType: 'module'
    },
    env: {
        browser: true,
    },
    plugins: [
        'react',
        'react-hooks'
    ],
    extends: [
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:react/jsx-runtime',
        'plugin:react-hooks/recommended'
    ],
    rules: {
        'react/no-unescaped-entities': 0
    },
    settings: {
        react: {
            version: 'detect'
        }
    }
};
